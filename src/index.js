import React from 'react';
import ReactDOM from 'react-dom';
import './index.css';
import App from './containers/App';
import registerServiceWorker from './registerServiceWorker';
import {getMasterRef, getNavigation, getContent} from "./data/contentHandler";

getMasterRef().then(masterRef => {
    const isSessionStorage = window.sessionStorage !== undefined;
    const cachedMasterRef = window.sessionStorage.getItem("masterRef");

    if ((isSessionStorage && !cachedMasterRef) || (isSessionStorage && cachedMasterRef !== masterRef)) {
        console.log("Ref difference");
        window.sessionStorage.setItem("masterRef", masterRef);
        getNavigation().then(navigation => {
            Promise.all(navigation.custom_page_array).then(res => {
                navigation.custom_page_array = res.map(res => {
                    return res;
                });
                getContent("homepage").then(data => {
                    const home = data.data;
                    isSessionStorage && window.sessionStorage.setItem("data", JSON.stringify({
                        navigation,
                        home
                    }));
                    ReactDOM.render(<App navigation={navigation} home={home}/>, document.getElementById('root'));
                    registerServiceWorker();
                })
            });
        });
    } else if (isSessionStorage && window.sessionStorage.getItem("masterRef") === masterRef && window.sessionStorage.getItem("data")) {
        console.log("Ref identical");
        const data = JSON.parse(window.sessionStorage.getItem("data"));
        ReactDOM.render(<App navigation={data.navigation} home={data.home}/>, document.getElementById('root'));
        registerServiceWorker();
    } else {
        console.log("No ref");
        getNavigation().then(navigation => {
            getContent("homepage").then(data => {
                const home = data.data;
                ReactDOM.render(<App navigation={navigation} home={home}/>, document.getElementById('root'));
                registerServiceWorker();
            })
        });
    }
});