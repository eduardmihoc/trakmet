import React, {Component} from "react";
import {withRouter} from "react-router-dom";

import ContentHandler from "../../data/contentHandler";
import ProductList from "../../components/ProductList/ProductList";

class ProductsPage extends Component {
    state = {
        productList: []
    };

    componentDidMount() {
        try {
            this.props.content.then(res => {
                this.setState(() => {
                    return {
                        productList: res
                    }
                });
            })
        } catch (err) {

        }
    }

    render() {
        const productsList = this.state.productList;
        const path = this.props.match.path;

        return (
            <div className="products">
                <ul>
                    <ProductList products={productsList} path={path}/>
                </ul>
            </div>
        )
    }
}

export default withRouter(ProductsPage);