import React, {Component} from 'react';
import {Route, Switch, HashRouter} from "react-router-dom";
//3rd party

//Error handling

//Core
import './App.css';

//Custom components
import AUX from "../hoc/auxiliary";
import withClass from "../hoc/withClass";
import Header from "../components/Header/Header";
import Footer from "../components/Footer/Footer";
import Homepage from "./Home/Home";
import AboutUs from "./AboutUs/AboutUs";
import Financing from "./Financing/Financing";
import CustomPage from "./CustomPage/CustomPage";
import Contact from "./Contact/Contact";
import ModalComponent from "../components/modal/modalComponent";
//Data
import {getContent} from "../data/contentHandler";
const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);
const className = isMobile ? "App mobile" : "App";

export const AuthContext = React.createContext(false);

class App extends Component {
    state = {
        homePage: this.props.home,
        productsPage: getContent("products"),
        aboutUsPage: getContent("about_us"),
        financingPage: getContent("financing"),
        contactPage: getContent("contact"),
        navigation: this.props.navigation,
        showModal: false,
        modalContent: null
    };

    render() {
        const {showModal, modalContent} = this.state;
        return (
            <HashRouter>
                <AUX>
                    {
                        showModal &&
                        <ModalComponent onClose={this.closeModal.bind(this)} HTML={modalContent}/>
                    }
                    <Header data={this.state.navigation.header} custom_pages={this.state.navigation.custom_page_array}
                            logo={this.state.homePage.logo.url}/>
                    <div className="content">
                        <Switch>
                            <Route path="/" exact component={() => <AboutUs content={this.state.aboutUsPage}/>}/>
                            {
                                this.state.navigation.custom_page_array.length &&
                                this.state.navigation.custom_page_array.map((res, index) => {
                                    return <Route path={"/" + res.label.trim().replace(" ", "_").toLowerCase()}
                                                  key={index + Math.random()}
                                                  component={() => <CustomPage data={res} openModal={this.openModal.bind(this)}/>}/>;

                                })
                            }
                            {/*<Route path="/our-network" component={() => <Homepage content={this.state.homePage}/>}/>*/}
                        </Switch>
                    </div>
                    <Route path="/contact" component={() => <Contact content={this.state.contactPage}/>}/>
                    <Footer data={this.state.navigation.footer}/>
                </AUX>
            </HashRouter>
        );
        // return React.createElement('div', {className: 'App'}, React.createElement('h1', null, 'Hello in H1'));
    }

    openModal = (content) => {
        this.setState({
            showModal: true,
            modalContent: content
        })
    };

    closeModal = () => {
        console.log("test");
        this.setState({
            showModal: false
        })
    };

    toggleModal = () => {
        this.setState({
            showModal: !this.state.showModal
        })
    }
}

//higher order component - X
export default withClass(App, className);
