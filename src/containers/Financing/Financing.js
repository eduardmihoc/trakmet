import React, {Component} from "react";
import {withRouter} from "react-router-dom";

import "./Financing.css";

const sectionBuilder = (item, index) => {
    switch (item.slice_type) {
        case "simple":
            return (
                <div key={index + Math.random()} className="simple">
                    <img src={item.primary.logo_or_image.url} alt=""/>
                    <div className="simple-content">
                        <h2>{item.primary.title1[0].text}</h2>
                        {
                            item.primary.description.map((res, index) => {
                                return <p key={index + Math.random()}>{res.text}</p>
                            })
                        }
                        <a className="simple-button" href={item.primary.cta_url[0].text}>{item.primary.cta_label[0].text}</a>
                    </div>
                </div>
            );
        case "faq":
            return (
                <div key={index + Math.random()} className="faq">
                    <h1>{item.primary.faq_title[0].text}</h1>
                    <ol>
                        {
                            item.items.map((res, index) => {
                                return (
                                    <li key={index + Math.random()}>
                                        <h2>{res.question[0].text}</h2>
                                        {
                                            res.answer.map((res, index) =>{
                                                return <p key={index + Math.random()}>{res.text}</p>
                                            })
                                        }
                                    </li>
                                )
                            })
                        }
                    </ol>
                </div>
            );
        default: return <h1>Content Unavailable</h1>
    }
};

class Financing extends Component {
    state = {
        data: {}
    };

    componentDidMount() {
        try {
            this.props.content.then(res => {
                this.setState(() => {
                    return {
                        data: res
                    }
                });
            })
        } catch (err) {

        }
    }

    render() {
        const data = this.state.data;
        return (
            <div className="financing">
                {
                    data.title && (
                        <div className="main-area">
                            <h1>{data.title[0].text}</h1>
                            <h2>{data.subtitle[0].text}</h2>
                            <img src={data.hero_image.url} alt=""/>
                        </div>
                    )
                }

                {
                    data.body &&
                    data.body.map((res, index) => {
                        return sectionBuilder(res, index);
                    })
                }
            </div>
        )
    }
}

export default withRouter(Financing);