import React from "react";
import OfferForm from "../../../../components/offer-form/offer-form";
import "./default-card.css";

const CardItem = (props) => (
    <a className="card" href={props.item.offerObject.url} target="_blank" onClick={() => {/*openModal(<OfferForm/>)*/
    }}>
        {
            props.item.title &&
            props.item.title.length &&
            <h2>{props.item.title[0].text}</h2>
        }
        <div className="details-section">
            <ul>
                {
                    props.item.listItems.length &&
                    props.item.listItems.map((res, index) => {
                        const title = res.label ? res.label[0].text : res.title[0].text;
                        let show = res.show === "True" ? "block" : "none";
                        show = res.show ? show : "block";
                        return (
                            <li style={{display: show}} key={index + Math.random()}>
                                <span>{title + ": " + res.description[0].text}</span>
                            </li>)
                    })

                }
            </ul>
            <p>{props.item.offerObject[0].text}</p>
        </div>
    </a>
);

export const DefaultCard = (props) => {
    const {offerObject, listItems, title} = props;
    const cardObject = {
        offerObject: offerObject || {},
        listItems: listItems || {},
        title: title
    };
console.log(cardObject);
    return (
        <div className="default-card">
            <CardItem item={cardObject}/>
        </div>
    )
};