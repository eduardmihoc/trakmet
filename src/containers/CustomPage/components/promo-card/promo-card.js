import React from "react";
import OfferForm from "../../../../components/offer-form/offer-form";
import "./promo-card.css";

const CardItem = (props) => (
    <a className="card" href={props.item.offerObject.url} target="_blank" onClick={() => {/*openModal(<OfferForm/>)*/
    }}>
        <h2>{props.item.title[0].text}</h2>
        <div className="details-section">
            <ul>
                {
                    props.item.listItems.length &&
                    props.item.listItems.map((res, index) => {
                        const title = res.label ? res.label[0].text : res.title[0].text;
                        let show = res.show === "True" ? "grid" : "none";
                        show = res.show ? show : "grid";
                        return (
                            <li style={{display: show}} key={index + Math.random()}>
                                <span className="title">{title}</span>
                                <span>{res.description[0].text}</span>
                            </li>)
                    })

                }
            </ul>
            <p>{props.item.offerObject[0].text}</p>
        </div>
    </a>
);

export const PromoCard = (props) => {
    const {offerObject, listItems, title} = props;
    const cardObject = {
        offerObject: offerObject || {},
        listItems: listItems || {},
        title: title
    };

    return (
        <div className="promo-card">
            <CardItem item={cardObject}/>
        </div>
    )
};