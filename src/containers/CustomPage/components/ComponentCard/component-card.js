import React, {Component} from "react";
import "./component-card.css";

export class ComponentCard extends Component {
    state = {
        showMore: false
    };

    render() {
        const {item} = this.props;
        const {showMore} = this.state;
        return (
            <div className="component-card">
                <div className="item">
                    <img src={item.image_left.url} alt={item.description[0].text}/>
                    <div>
                        <span>{item.title[0].text}</span>
                        <a onClick={(e) => {
                            e.preventDefault();
                            this.setState({
                                showMore: !this.state.showMore
                            })
                        }}>{showMore ? "-" : "+"}</a>
                    </div>
                    {
                        showMore &&
                        <div>
                            <p>{item.description[0].text}</p>
                            <img src={item.image_right.url} alt={item.description[0].text}/>
                        </div>
                    }
                </div>
            </div>
        )
    }
};