import React from "react";
import OfferForm from "../../../../components/offer-form/offer-form";
import "./card.css";

const tableMapping = {
    // 0: "model",
    // 1: "volant",
    // 3: "lungime",
    0: "latime",
    1: "motor",
    2: "randament",
    3: "oferta"
};
export const Card = (props) => {
    const {table_header_translations, components_table, offerObject, openModal, title} = props;
    return (
        <a className="card" href={components_table[tableMapping[3]].url} target="_blank"  onClick={() => openModal(<OfferForm/>)}>
            <h2>{title[0].text}</h2>
            <div className="details-section">
                <ul>
                    {
                        table_header_translations.length &&
                        table_header_translations.map((res, index) => {
                            const value = Array.isArray(components_table[tableMapping[index]]) ? components_table[tableMapping[index]][0].text : false;
                            return (
                                <li style={{display: !value && "none"}} key={index + Math.random()}>
                                    <span>{res.text + ": " + value}</span>
                                </li>
                            )
                        })
                    }
                </ul>
                <p>{offerObject.text}</p>
            </div>
        </a>
    )
};