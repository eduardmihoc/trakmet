import React, {Fragment} from "react";
import {Carousel} from "react-responsive-carousel";

import "./description-component.css"

export const DescriptionComponent = (props) => {
    const {res, autoPlayInterval, noCarousel} = props;
    return (
        <div className="description-component">
            <h2>{res.primary.title[0].text}</h2>
            <div className="description">
                {
                    res.primary.description[0] &&
                    res.primary.description.length &&
                    res.primary.description.map((res, index) => {
                        return <p
                            key={index + Math.random()}/* style={{fontSize: res.text.length < 30 ? "18px" : "auto", fontWeight: res.text.length < 30 ? "bold" : "normal"}}*/>{res.text}</p>
                    })
                }
            </div>
            <div className="gallery">
                {
                    noCarousel ?
                        <Fragment>
                            {
                                res.items.length > 1 &&
                                res.items.map((res, index) => {
                                    return (
                                        <div className="carousel-item"
                                             key={index + Math.random()}>
                                            <img src={res.images.url}
                                                 alt="gallery-item"/>
                                        </div>
                                    )
                                })
                            }
                        </Fragment> :
                        <Carousel showArrows={true} className="carousel"
                                  infiniteLoop={true} autoPlay={true}
                                  interval={autoPlayInterval}
                                  transitionTime={750}
                                  showThumbs={false}>
                            {
                                res.items.length > 1 &&
                                res.items.map((res, index) => {
                                    return (
                                        <div className="carousel-item"
                                             key={index + Math.random()}>
                                            <img src={res.images.url}
                                                 alt="gallery-item"/>
                                        </div>
                                    )
                                })
                            }
                        </Carousel>
                }

            </div>
        </div>
    )
}
