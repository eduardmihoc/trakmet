import React from "react";

import "./component-item.css";

export const ComponentItem = (props) => {
    const { res } = props;
    return (
        <div className={!res.image_left.url && !res.image_right.url ? "component-item no-image": "component-item"}>
            {res.image_left.url && <img src={res.image_left.url} alt={res.title[0].text}/>}
            <div>
                {res.title[0] && res.title[0].text && <span>{res.title[0].text}</span>}
                {
                    res.description.length &&
                        res.description.map((item, index) => {
                            return (
                                <p key={index + Math.random()}>{item.text}</p>
                            )
                        })
                }
            </div>
            {res.image_right.url && <img src={res.image_right.url} alt={res.title[0].text}/>}
        </div>
    )
};