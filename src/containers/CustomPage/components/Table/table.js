import React from "react";
import OfferForm from "../../../../components/offer-form/offer-form";

import "./table.css";

export const Table = (props) => {
    const table_header_translations = props.table_header_translations;
    const components_table = props.components_table;
    return (
        <div className="offer-table">
            <table>
                {
                    table_header_translations &&
                    table_header_translations.length &&
                    <thead>
                    <tr>
                        {
                            table_header_translations.map((res, index) => {
                                return <th key={index + Math.random()}>{res.text}</th>
                            })
                        }
                    </tr>
                    </thead>
                }
                <tbody>
                {
                    components_table.length &&
                    components_table.map((res, index) => {
                        const objKeys = Object.keys(res);

                        return (
                            <tr key={index + Math.random()}>
                                {
                                    objKeys.map((key, index) => {
                                        const mainObj = res[key];
                                        if(key === "show") {
                                            return <td key={index + Math.random()} style={{display: "none"}}>Empty</td>;
                                        }
                                        return mainObj.length ?
                                            <td key={index + Math.random()}>{res[key][0].text}</td> :
                                            <td key={index + Math.random()}><a href={res[key].url} target="_blank" onClick={() => props.openModal(<OfferForm/>)}>Descarca oferta</a></td>;
                                    })
                                }
                            </tr>
                        )
                    })
                }
                </tbody>
            </table>
        </div>
    )
};
