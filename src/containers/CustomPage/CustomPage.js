import React, {Component, Fragment} from "react";
import {withRouter} from "react-router-dom";
import CoverFlow from 'coverflow-react-minor-tweaks';
import {Carousel} from 'react-responsive-carousel';

import {Table} from "./components/Table/table";
import {Card} from "./components/Card/card";
import {DefaultCard} from "./components/default-card/default-card";
import {PromoCard} from "./components/promo-card/promo-card";
import {ComponentItem} from "./components/ComponentItem/component-item";
import {DescriptionComponent} from "./components/description-component/description-component";
import OfferForm from "../../components/offer-form/offer-form";

import "react-responsive-carousel/lib/styles/carousel.min.css";
import "./CustomPage.css";

const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

const PageBuilder = (page_data, openModal) => {
    switch (page_data.data.custom_page_type) {
        case "banzice":
            return (
                <div className="banzice">
                    {
                        page_data.data.body &&
                        page_data.data.body.map((res, index) => {
                            switch (res.slice_type) {
                                case "carousel":
                                    const autoPlayInterval = res.items.length <= 2 ? 7500 : 3000;
                                    const imgArray = res.items.map(res => res.image.url);
                                    const labelArray = res.items.map(res => res.description_title[0] ? res.description_title[0].text : "");
                                    return (
                                        <Fragment key={index + Math.random()}>
                                            {
                                                isMobile ?
                                                    <Fragment>
                                                        {
                                                            res.primary.title.length &&
                                                            <h1>{res.primary.title[0].text}</h1>
                                                        }
                                                        <Carousel showArrows={true} className="carousel"
                                                                  infiniteLoop={true}
                                                                  autoPlay={true}
                                                                  interval={autoPlayInterval} transitionTime={750}
                                                                  showThumbs={false}>
                                                            {
                                                                res.items.length &&
                                                                res.items.map((res, index) => {
                                                                    return (
                                                                        <div className="carousel-item"
                                                                             key={index + Math.random()}>
                                                                            <img src={res.image.url}
                                                                                 alt={res.description_title[0] ? res.description_title[0].text : ""}/>
                                                                            {
                                                                                res.description_title.length > 1 &&
                                                                                <p className="legend">{res.description_title[0].text}</p>
                                                                            }
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                        </Carousel>
                                                    </Fragment> :
                                                    <Fragment>
                                                        {
                                                            res.primary.title.length &&
                                                            <h1>{res.primary.title[0].text}</h1>
                                                        }
                                                        <CoverFlow imagesArr={imgArray} labelsArr={labelArray}
                                                                   background="#f0f8ff08"
                                                                   height={500}
                                                                   itemRatio="8:7"/>
                                                    </Fragment>
                                            }
                                        </Fragment>
                                    );
                                case "components":
                                    console.log(res);
                                    return (
                                        <div className="component-container" key={index + Math.random()}>
                                            {
                                                <h2 className="modal-header" onClick={() => isMobile && openModal(
                                                    <Fragment>
                                                        {
                                                            res.items.length > 1 &&
                                                            res.items.map((res, index) => {
                                                                return (
                                                                    <ComponentItem res={res}
                                                                                   key={index + Math.random()}/>
                                                                )
                                                            })
                                                        }
                                                    </Fragment>
                                                )}>{res.primary.title[0].text}<span>{isMobile ? " > " : ""}</span></h2>
                                            }
                                            {
                                                isMobile ?
                                                    "" :
                                                    res.items.length > 1 &&
                                                    res.items.map((res, index) => {
                                                        return (
                                                            <div
                                                                className={!res.image_left.url && !res.image_right.url ? "component-item no-image" : "component-item"}
                                                                key={index + Math.random()}>
                                                                {res.image_left.url &&
                                                                <img src={res.image_left.url} alt={res.title[0].text}/>}
                                                                <div>
                                                                    {res.title[0] && <span>{res.title[0].text}</span>}
                                                                    {
                                                                        res.description.length &&
                                                                        res.description.map((item, index) => {
                                                                            return (
                                                                                <p key={index + Math.random()}>{item.text}</p>
                                                                            )
                                                                        })
                                                                    }
                                                                </div>
                                                                {res.image_right.url && <img src={res.image_right.url}
                                                                                             alt={res.title[0].text}/>}
                                                            </div>
                                                        )
                                                    })
                                            }
                                        </div>
                                    );
                                case "table":
                                    const offerObject = res.primary.table_translations[res.primary.table_translations.length - 1];
                                    return (
                                        <div className="table-container" key={index + Math.random()}>
                                            {
                                                res.primary.title &&
                                                res.primary.title[0].length &&
                                                <h2>{res.primary.title[0].text}</h2>
                                            }
                                            {
                                                isMobile ?
                                                    res.items.length &&
                                                    res.items.map((item, index) => {
                                                        const translations = [res.primary.table_translations[5], res.primary.table_translations[2], res.primary.table_translations[4], res.primary.table_translations[6]];
                                                        const itemData = {
                                                            latime: item.latime,
                                                            motor: item.motor,
                                                            randament: item.randament,
                                                            oferta: item.oferta
                                                        };

                                                        return <Card key={index + Math.random()}
                                                                     title={item.model}
                                                                     table_header_translations={translations}
                                                                     components_table={itemData}
                                                                     offerObject={offerObject}
                                                                     openModal={openModal}/>
                                                    }) :
                                                    <Table
                                                        table_header_translations={res.primary.table_header_translations}
                                                        components_table={res.items} openModal={openModal}/>
                                            }
                                        </div>
                                    );
                                case "video":
                                    return (
                                        <div key={index + Math.random()} className="video">
                                            <h2>{res.primary["video_title"][0] ? res.primary["video_title"][0].text : ""}</h2>
                                            <div className={"video-container video-container__" + res.items.length}
                                                 key={index + Math.random()}>
                                                {
                                                    res.items.length &&
                                                    res.items.map((item, index) => {
                                                        return (
                                                            <iframe className="video-iframe" title="video-iframe"
                                                                    src={item["video_link"].url} frameBorder="0"
                                                                    key={index + Math.random()}/>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    );
                                default:
                                    return <h1>No content!</h1>

                            }
                        })
                    }
                </div>
            );
        case "circulare":
            return (
                <div className="circulare">
                    {
                        page_data.data.body.length &&
                        page_data.data.body.map((res, index) => {
                            switch (res.slice_type) {
                                case "carousel":
                                    const autoPlayInterval = res.items.length <= 2 ? 7500 : 3000;
                                    const imgArray = res.items.map(res => res.image.url);
                                    const labelArray = res.items.map(res => res.title[0] ? res.title[0].text : "");
                                    return (
                                        <Fragment key={index + Math.random()}>
                                            {
                                                isMobile ?
                                                    <Fragment>
                                                        {
                                                            res.primary.title.length &&
                                                            <h1>{res.primary.title[0].text}</h1>
                                                        }
                                                        <Carousel showArrows={true} className="carousel"
                                                                  infiniteLoop={true}
                                                                  autoPlay={true}
                                                                  interval={autoPlayInterval} transitionTime={750}
                                                                  showThumbs={false}>
                                                            {
                                                                res.items.length &&
                                                                res.items.map((item, index) => {
                                                                    return (
                                                                        <div className="carousel-item"
                                                                             key={index + Math.random()}>
                                                                            <img src={item.image.url}
                                                                                 alt={item.title[0] && item.title[0].text}/>
                                                                            {
                                                                                item.title.length > 1 &&
                                                                                <p className="legend">{item.title[0].text}</p>
                                                                            }
                                                                        </div>
                                                                    )
                                                                })
                                                            }
                                                        </Carousel>
                                                    </Fragment> :
                                                    <Fragment>
                                                        {
                                                            res.primary.title.length &&
                                                            <h1>{res.primary.title[0].text}</h1>
                                                        }
                                                        <CoverFlow imagesArr={imgArray} labelsArr={labelArray}
                                                                   background="#f0f8ff08" height={500}
                                                                   itemRatio="8:7"/>
                                                    </Fragment>
                                            }
                                        </Fragment>
                                    );
                                case "circular":
                                    return (
                                        <div className="circular" key={index + Math.random()}>
                                            {
                                                isMobile ?
                                                    <h2 className="modal-header"
                                                        onClick={() => openModal(<DescriptionComponent res={res}
                                                                                                       noCarousel={true}
                                                                                                       autoPlayInterval={autoPlayInterval}/>)}>{res.primary.title[0] && res.primary.title[0].text}<span>{isMobile ? " > " : ""}</span>
                                                    </h2> :
                                                    <Fragment>
                                                        <h2>{res.primary.title[0].text}</h2>
                                                        <div className="description">
                                                            {
                                                                res.primary.description[0] &&
                                                                res.primary.description.length &&
                                                                res.primary.description.map((res, index) => {
                                                                    return <p key={index + Math.random()}>{res.text}</p>
                                                                })
                                                            }
                                                        </div>
                                                        <div className="gallery">
                                                            {
                                                                isMobile ?
                                                                    <Carousel showArrows={true} className="carousel"
                                                                              infiniteLoop={true} autoPlay={true}
                                                                              interval={autoPlayInterval}
                                                                              transitionTime={750}
                                                                              showThumbs={false}>
                                                                        {
                                                                            res.items.length > 1 &&
                                                                            res.items.map((res, index) => {
                                                                                return (
                                                                                    <div className="carousel-item"
                                                                                         key={index + Math.random()}>
                                                                                        <img src={res.images.url}
                                                                                             alt="gallery-item"/>
                                                                                    </div>
                                                                                )
                                                                            })
                                                                        }
                                                                    </Carousel>
                                                                    :
                                                                    res.items &&
                                                                    res.items.map((res, index) => {
                                                                        return <img key={index + Math.random()}
                                                                                    src={res.images.url}
                                                                                    alt=""/>
                                                                    })
                                                            }
                                                        </div>
                                                    </Fragment>
                                            }

                                        </div>
                                    );
                                case "table":
                                    return (
                                        <Fragment key={index + Math.random()}>
                                            {
                                                isMobile ?
                                                    <DefaultCard
                                                        offerObject={Object.assign({}, res.primary.oferta_label, res.primary.oferta_document)}
                                                        title={res.primary.title} listItems={res.items}/> :
                                                    <div className="circular-table" key={index + Math.random()}>
                                                        {/*<h2>{res.primary.title[0].text}</h2>*/}
                                                        {/*<Table components_table={res.items}/>*/}
                                                        {
                                                            res.primary.oferta_label[0] &&
                                                            <div className="offer-pitch">
                                                                <a href={res.primary.oferta_document.url}
                                                                   target="_blank"
                                                                   onClick={() => openModal(
                                                                       <OfferForm/>)}>{res.primary.oferta_label[0].text}</a>
                                                            </div>
                                                        }
                                                    </div>
                                            }
                                        </Fragment>
                                    );
                                case "video":
                                    return (
                                        <div key={index + Math.random()} className="video">
                                            <h2>
                                                {
                                                    res.primary["video_title"][0] &&
                                                    res.primary["video_title"][0].text
                                                }
                                            </h2>
                                            <div className={"video-container video-container__" + res.items.length}
                                                 key={index + Math.random()}>
                                                {
                                                    res.items.length &&
                                                    res.items.map((res, index) => {
                                                        return (
                                                            <iframe className="video-iframe" title="video-iframe"
                                                                    src={res["video_link"].url} frameBorder="0"
                                                                    key={index + Math.random()}/>
                                                        )
                                                    })
                                                }
                                            </div>
                                        </div>
                                    );
                                default:
                                    return <h1>No content</h1>
                            }
                        })
                    }
                </div>
            );
        case "promo":
            return (
                <div className="promo">
                    <h2>{page_data.data.title[0].text}</h2>
                    <div className="description">
                        {
                            page_data.data.description &&
                            page_data.data.description.length &&
                            page_data.data.description.map((res, index) => {
                                return <p key={index + Math.random()}>{res.text}</p>
                            })
                        }
                    </div>
                    <div className="promo-container">
                        {
                            page_data.data.body.length &&
                            page_data.data.body.map((res, index) => {
                                console.log(page_data.data);
                                return (
                                    <PromoCard key={index + Math.random()}
                                               offerObject={Object.assign({}, res.primary.offer_label, res.primary.offer_url)}
                                               title={res.primary.promo_title} listItems={res.items}/>
                                )
                            })
                        }
                    </div>
                </div>
            );
        default:
            return <h1>No content</h1>
    }
};

class CustomPage extends Component {
    state = {
        data: {}
    };

    componentDidMount() {
        window.scroll(0, 0);
    }

    componentWillUnmount() {
    }

    render() {
        const page_data = this.props.data;
        const {openModal} = this.props;
        const view = PageBuilder(page_data, openModal);
        ;
        return (
            <div className="custom_page">
                {view}
            </div>
        )
    }
}

export default withRouter(CustomPage);