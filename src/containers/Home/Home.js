import React, {Component} from "react";
import {withRouter} from "react-router-dom";
import Section from "../../components/Sections/Section";

import "./Home.css";

class HomePage extends Component {
    state = {
        data: this.props.content
    };

    render() {
        const homeSections = this.state.data.sections;
        return (
            <div className="homepage">
                {
                    homeSections.length &&
                    homeSections.map((item, index) => {
                        return (
                            index >= 0 ?
                                <Section item={item} key={index + Math.random()}/>
                                : ""
                        )
                    })
                }
            </div>
        )
    }
}

export default withRouter(HomePage);