import React, {Component} from "react";

import {getProductContent} from "../../data/contentHandler";
import { FaThumbsUp, FaEuroSign, FaTruck, FaCogs } from 'react-icons/fa';
import "./Product.css";

const IconMap = {
    FaThumbsUp: <FaThumbsUp className="fa-icon"/>,
    FaEuroSign: <FaEuroSign className="fa-icon"/>,
    FaTruck: <FaTruck className="fa-icon"/>,
    FaCogs: <FaCogs className="fa-icon"/>,
};

const sectionBuilder = (sectionItem) => {
    let section;
    switch (sectionItem.slice_type) {
        case "hero":
            return (
                <div key={Math.random()} className="product-hero">
                    <h1>{sectionItem.primary["hero-list-title"][0].text}</h1>
                    <div className="primary">
                        <h2>{sectionItem.primary["hero-list-subtitle"][0].text}</h2>
                        {sectionItem.items.map((res, index) => {
                            return (
                                <div className="list-container" key={index + Math.random()}>
                                    <h4>{res.hero_list_title[0].text}</h4>
                                    <ul>
                                        {
                                            res["hero-list-details"].map((res, index) => {
                                                return <li key={index + Math.random()}>{res.text}</li>
                                            })
                                        }
                                    </ul>
                                </div>
                            )
                        })}
                    </div>
                    <div className="secondary">
                        <h2>{sectionItem.primary["hero-title"][0].text}</h2>
                        <p>{sectionItem.primary["hero-description"][0].text}</p>
                        <img src={sectionItem.primary["hero-image"].url} alt=""/>
                    </div>
                </div>
            );
        case "options":
            return (
                <div key={Math.random()} className="product-options">
                    <div className="primary">
                        <h2>{sectionItem.primary["options-title"][0].text}</h2>
                        <ol>
                            {sectionItem.primary["options-list"].map((res, index) => {
                                return (
                                    <li key={index + Math.random()}>
                                        {res.text}
                                    </li>
                                )
                            })}
                        </ol>
                        <div className="gallery-container">
                            {
                                sectionItem.items.map((res, index) => {
                                    return (
                                        <div className="image-container" key={index + Math.random()}>
                                            <figure>
                                                <img src={res["option-image"].url} alt=""/>
                                                <figcaption>{res["option-caption"][0].text}</figcaption>
                                            </figure>
                                        </div>
                                    )
                                })
                            }
                        </div>
                        <h3>{sectionItem.primary["options-pricing"][0].text}</h3>
                    </div>
                </div>
            );
        case "auxiliary":
            return (
                <div key={Math.random()} className="auxiliary">
                    <h2>{sectionItem.primary["auxiliary-title"][0].text}</h2>
                    <ul>
                        {
                            sectionItem.primary["auxiliary-list"].map((res, index) => {
                                return (
                                    <li key={index + Math.random()}>{res.text}</li>
                                )
                            })
                        }
                    </ul>
                    <a href={sectionItem.primary["auxiliary-document"].url} target="_blank">{sectionItem.primary["auxiliary-cta"][0].text}</a>
                </div>
            );
        case "auxiliary-plus":
            return (
                <div key={Math.random()} className="auxiliary-plus">
                    <h2>{sectionItem.primary["auxiliary-plus-title"][0].text}</h2>
                    <div className="auxiliary-list">
                        {
                            sectionItem.items.map((res, index) => {
                                return (
                                    <div className="auxiliary-item" key={index + Math.random()}>
                                        <h3>{res["auxiliary-item-title"][0].text}</h3>
                                        <img src={res["auxiliary-item-image"].url} alt=""/>
                                        <ul>
                                            {
                                                res["auxiliary-item-list"].map((res, index) => {
                                                    return (
                                                        <li key={index + Math.random()}>{res.text}</li>
                                                    )
                                                })
                                            }
                                        </ul>
                                        <p>{res["auxiliary-item-price"][0].text}</p>
                                    </div>
                                )
                            })
                        }
                    </div>
                </div>
            );
        case "video":
            return (
                <div key={Math.random()} className="video">
                    <h2>{sectionItem.primary["video-title"][0].text}</h2>
                    {
                        sectionItem.items.map((res, index) => {
                            return (
                                <div className="video-container" key={index + Math.random()}>
                                    <h3>{res["video-item-title"][0].text}</h3>
                                    <iframe className="video-iframe" title="video-iframe" src={res["video-item-url"].url} frameBorder="0"/>
                                </div>
                            )
                        })
                    }
                </div>
            );
        case "fyi":
            return (
                <div key={Math.random()} className="fyi">
                    <h2>{sectionItem.primary["fyi-title"][0].text}</h2>
                    <ul>
                        {
                            sectionItem.items.map((res, index) => {
                                return (
                                    <li key={index + Math.random()}>
                                        <span>{IconMap[res["fyi-item"]]}{res["fyi-text"][0].text}</span>
                                    </li>
                                )
                            })
                        }
                    </ul>
                </div>
            );
        case "photo-gallery":
            return (
                <div key={Math.random()} className="photo-gallery">
                    <h2>{sectionItem.primary["gallery-title"][0].text}</h2>
                    <div className="gallery-container">
                        {sectionItem.items.map((res, index) => {
                            return (
                                <img key={index + Math.random()} src={res["gallery-item-image"].url} alt=""/>
                            )
                        })}
                    </div>
                </div>
            );

        default:
            section = <p>Test</p>;
    }
    return section;
};

class ProductPage extends Component {
    state = {
        productSections: []
    };

    componentDidMount() {
        //TODO solve deeplink redirect issue on live server
        getProductContent(this.props.match.params.id).then(res => {
            this.setState(() => {
                return {
                    productSections: res.body
                }
            })
        })
    }

    render() {
        return (
            <div className="product">
                {
                    this.state.productSections.map(res => {
                        return sectionBuilder(res);
                    })
                }
            </div>
        )
    }
}

export default ProductPage;