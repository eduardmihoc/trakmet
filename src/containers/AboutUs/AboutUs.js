import React, {Component} from "react";
import {withRouter} from "react-router-dom";

import "./AboutUs.css";

class AboutUs extends Component {
    state = {
        data: {}
    };

    componentDidMount() {
        try {
            this.props.content.then(res => {
                this.setState(() => {
                    return {
                        data: res
                    }
                });
            })
        } catch (err) {

        }
    }

    render() {
        const data = this.state.data;
        return (
            <div className="about-us">
                {
                    data.title &&
                    <h1>{data.title[0].text}</h1>
                }
                {
                    data.body &&
                    <div className="data-container">
                        {
                            data.body.map((res, index) => {
                                return (
                                    <div key={index + Math.random()} className="section-container">
                                        <h2>{res.primary.subtitle[0].text}</h2>
                                        {
                                            res.items.map((res, index) => {
                                                const paragraphs = res.paragraph.map((res, index) => {
                                                    return (
                                                        <p key={index + Math.random()}>{res.text}</p>
                                                    )
                                                });

                                                return (
                                                    <div key={index + Math.random()} className="">
                                                        {paragraphs}
                                                        {
                                                            res.image.url &&
                                                            <img src={res.image.url} alt=""/>
                                                        }
                                                    </div>
                                                )

                                            })
                                        }
                                    </div>
                                )
                            })
                        }
                    </div>
                }


            </div>
        )
    }
}

export default withRouter(AboutUs);