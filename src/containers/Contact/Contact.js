import React, {Component, Fragment} from "react";
import {withRouter} from "react-router-dom";

import "./Contact.css";

const token = "0abd677f-356b-4b36-a798-d85da76c5eee";

class Contact extends Component {
    state = {
        data: {},
        formData: {
            name: '',
            email: '',
            company: "",
            message: ""
        }
    };

    componentDidMount() {
        try {
            this.props.content.then(res => {
                this.setState(() => {
                    return {
                        data: res
                    }
                });
            })
        } catch (err) {

        }
    }

    render() {
        const {data, formData} = this.state;
        return (
            <div className="contact">
                {
                    data.title &&
                    <Fragment>
                        <h1>{data.title[0].text}</h1>
                        <div className="contact-details">
                            <img src={data.hq_image.url} alt=""/>
                            <div className="details-description">
                                <h2>{data.hq_adress_title[0].text}</h2>
                                {
                                    data.hq_address_description.length &&
                                    data.hq_address_description.map((res, index) => {
                                        return (
                                            <p key={index + Math.random()}>{res.text}</p>
                                        )
                                    })
                                }
                            </div>
                        </div>
                        <div className="contact-person">
                            <div className="person-details">
                                <img src={data.contact_person_image.url} alt=""/>
                                <div className="person-description">
                                    <h2>{data.contact_person_title[0].text}</h2>
                                    <p>{data.contact_person_name[0].text}</p>
                                    <p>{data.contact_person_phone[0].text}</p>
                                    <p>{data.contact_person_email[0].text}</p>
                                </div>
                            </div>
                            <div className="map">
                                <iframe title="hq-map" className="map-iframe" frameBorder="0"
                                        src={data.contact_person_map.url}/>
                            </div>
                        </div>
                        <div className="contact-form">
                            <h2>{data.contact_form_title[0].text}</h2>
                            <div className="contact-form-elements">
                                <div className="form-inputs">
                                    <input type="text" placeholder={data.contact_form_name[0].text}
                                           value={this.state.formData.name} onChange={(e) => this.setState({
                                        formData: {
                                            name: e.target.value,
                                            email: formData.email,
                                            message: formData.message,
                                            company: formData.company
                                        }
                                    })}/>
                                    <input type="text" placeholder={data.contact_form_email[0].text}
                                           value={formData.email} onChange={(e) => this.setState({
                                        formData: {
                                            email: e.target.value,
                                            name: formData.name,
                                            company: formData.company,
                                            message: formData.message
                                        }
                                    })}/>
                                    <input type="text" placeholder={data.contact_form_company[0].text}
                                           value={formData.company} onChange={(e) => this.setState({
                                        formData: {
                                            email: formData.email,
                                            name: formData.name,
                                            company: e.target.value,
                                            message: formData.message
                                        }
                                    })}/>
                                    <button type="text"
                                            onClick={() => this.sendEmail()}>{data.contact_form_cta[0].text}</button>
                                </div>
                                <textarea name="message" id="message" cols="30" rows="1"
                                          placeholder={data.contact_form_message[0].text} value={formData.message}
                                          onChange={(e) => this.setState({
                                              formData: {
                                                  email: formData.email,
                                                  name: formData.name,
                                                  company: formData.company,
                                                  message: e.target.value
                                              }
                                          })}/>
                            </div>
                        </div>
                    </Fragment>
                }
            </div>
        )
    }

    sendEmail = () => {
        if (this.state.formData.name.length && this.state.formData.email.length) {
            window.Email.send("office@banzice.ro",
                "office@banzice.ro",
                "Contact - " + this.state.formData.name + " - " + this.state.formData.email + " - " + this.state.formData.company,
                this.state.formData.message,
                {token: token});
        }
    }
}

export default withRouter(Contact);