import React, {Component} from "react";
import Modal from 'react-modal';

const customStyles = {
    content: {
        position: 'relative',
        top: 'auto',
        left: 'auto',
        right: 'auto',
        bottom: 'auto',
        background: 'rgba(0, 0, 0, 0.8)',
        margin: '0 auto',
        overflow: "none",
        zIndex: "9999999"
    },
    overlay: {
        position: 'fixed',
        top: 0,
        left: 0,
        right: 0,
        bottom: 0,
        margin: '0 auto',
        backgroundColor: 'rgba(0, 0, 0, 0.5)',
        overflow: "scroll",
        display: "grid",
        alignItems: "center",
        zIndex: "9999999"
    },
};

export default class ModalComponent extends Component {
    constructor(props) {
        super(props);
        this.state = {
            currentLocation: window.location.href
        };
        this.backHandler = this.backHandler.bind(this);
    }
    componentDidMount() {
        // The popstate event is fired each time when the current history entry changes.
        window.addEventListener('popstate', this.backHandler, false);
    }

    componentWillUnmount() {
        window.removeEventListener('popstate', this.backHandler, false);
    }

    render() {
        const {onClose, HTML} = this.props;
        return (
            <Modal isOpen={true} onRequestClose={() => onClose()} ariaHideApp={false} style={customStyles}>
                <p className="modal-button" onClick={() => onClose()} style={{
                    width: "25px",
                    height: "25px",
                    color: "black",
                    fontWeight: "bold",
                    background: "white",
                    fontSize: "25px",
                    lineHeight: "23px",
                    textAlign: "center",
                    borderRadius: "5%"
                }}>x</p>
                {
                    HTML
                }
            </Modal>
        )
    }

    backHandler() {
        window.history.pushState(null, null, this.state.currentLocation);
        this.props.onClose();
    }
}