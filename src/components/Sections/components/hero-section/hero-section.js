import React from "react";

const HeroSection = (props) => {
    const {item} = props;
    return (
        <div className={"section-component " + item.type}>
            {/*<img className="logo" src={item["logo"].url  || "http://pluspng.com/img-png/tree-limb-png-tree-branch-png-transparent-image-2000.png"} alt=""/>*/}
            <div className="left">
                <h1>{item.title[0].text}</h1>
                <div className="pictures-container">
                    <img
                        src={item.info.items[0].url}
                        alt=""/>
                </div>
            </div>
            <div className="right">
                <span>{item.description[0].text}</span>
                {/*<button className={"button"}>{item.cta[0].text}</button>*/}
                <div className="aux-image">
                    <img
                        src={item["background-image"].url}
                        alt=""/>
                </div>
            </div>
        </div>
    )
};

export default HeroSection;