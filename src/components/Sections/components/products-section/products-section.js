import React from "react";
import {Link} from "react-router-dom";
const ProductsSection = (props) => {
    const {item} = props;

    const backToTOp = () => {
        document.documentElement.scrollTop = 0;
    };

    return (
        /*style={{backgroundImage: `url(${item.background})`}}*/
        <div className={"section-component products-list"}>
            <h2>{item.title[0].text}</h2>
            <div className="product-container">
                {
                    item.infoList.map((item, index) => {
                        const itemData = item.data;
                        return (
                            <Link onClick={backToTOp} to={"/product/" + item.id} className={"section-item"} key={index + Math.random()}>
                                <h2>{itemData.title[0].text}</h2>
                                <p>{itemData.short_description[0].text}</p>
                                <img
                                    src={itemData.small_image.url}
                                    alt=""/>
                            </Link>
                        )
                    })
                }
            </div>
        </div>
    )
};

export default ProductsSection;