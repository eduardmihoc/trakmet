import React, {Component} from "react";
import FlagDescription from "./components/flag-description"

class FlagsSection extends Component {
    state = {
        isActive: false
    };

    render() {
        const {item} = this.props;
        return (
            <div className={"section-component " + item.type}>
                <h2>{item.title[0].text}</h2>
                <div className="section-item-container">
                    {
                        item.infoList.map((item, index) => {
                            return (
                                <FlagDescription key={index + Math.random()} item={item} index={index} />
                            )
                        })
                    }
                </div>
            </div>
        )
    }
}

export default FlagsSection;