import React, {Component} from "react";

export default class FlagDescription extends Component {
    state = {
        isActive: false
    };

    render() {
        const {item, index} = this.props;
        return (
            <div className={this.state.isActive ? "section-item active" : "section-item"}
                 key={index + Math.random()}
                 onClick={() => this.setState(() => {
                     return {isActive: !this.state.isActive}
                 })}
                 onMouseLeave={() => this.setState(() => {
                     return {isActive: false}
                 })}>
                <span style={{backgroundImage: `url(${item.image.url})`, backgroundSize: "100% 100%"}}>
                    {/*<img
                        src={item.image.url}
                        alt=""/>*/}
                    <h2>{item.title[0].text}</h2>
                    <div className="description-container">
                        {
                            item.description &&
                            item.description.map((res, index) => {
                                return <p key={index + Math.random()}>{res.text}</p>
                            })
                        }
                    </div>
                </span>
            </div>
        )
    }
}