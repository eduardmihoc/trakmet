import React from "react";

const SimpleSection = (props) => {
    const {item} = props;
    return (
        <div className={"section-component " + item.type}>
            <h2>{item.title[0].text}</h2>
            <p>{item.description[0].text}</p>
            {
                item["background-image"] &&
                <div className="main-image">
                    <img
                        src={item["background-image"].url || "http://pluspng.com/img-png/tree-limb-png-tree-branch-png-transparent-image-2000.png"}
                        alt=""/>
                </div>
            }
            {
                item.listItems &&
                <ul className="description-list">

                    {
                        item.listItems.map((res, index) => {
                            return (
                                <li key={index + Math.random()}>
                                    <h3>{res.title1[0].text}</h3>
                                    <p>{res.description1[0].text}</p>
                                </li>
                            )
                        })
                    }
                </ul>
            }
        </div>
    )
};

export default SimpleSection;