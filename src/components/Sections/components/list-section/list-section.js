import React, {} from "react";
import {Link} from "react-router-dom";

const ListSection = (props) => {
    const {item} = props;

    const backToTOp = () => {
        document.documentElement.scrollTop = 0;
    };

    return (
        /*style={{backgroundImage: `url(${item.background})`}}*/
        <div className={"section-component " + item.type}>
            <div className="item-container">
                {
                    item.listItems.map((item, index) => {
                        return (
                            <div className={"section-item " + (index % 2 && "align-right")} key={index + Math.random()}>
                                <h2>{item.title1[0].text}</h2>
                                <p>{item.description1[0].text}</p>
                                {
                                    item.cta_url[0] &&
                                    <Link onClick={backToTOp} to={item.cta_url[0].text}>{item.cta1[0].text}</Link>
                                }
                                <div className="image-container">
                                    <img
                                        src={item.image.url}
                                        alt=""/>
                                </div>
                            </div>
                        )
                    })
                }
            </div>
        </div>
    )
};

export default ListSection;