import React, {Component} from "react";

import HeroSection from "./components/hero-section/hero-section";
import SimpleSection from "./components/simple-section/simple-section";
import ListSection from "./components/list-section/list-section";
import FlagSection from "./components/flags-section/flag-section";
import ProductSection from "./components/products-section/products-section";

import "./Section.css";

const sectionBuilder = (sectionItem) => {
    let section;
    switch (sectionItem.type) {
        case "hero":
            section = <HeroSection item={sectionItem}/>;
            break;
        case "simple":
            section = <SimpleSection item={sectionItem}/>;
            break;
        case "list":
            section = <ListSection item={sectionItem}/>;
            break;
        case "flags":
            section = <FlagSection item={sectionItem}/>;
            break;
        case "products":
            section = <ProductSection item={sectionItem}/>;
            break;
        default:
            section = <p>Test</p>;
    }
    return section;
};

class Section extends Component {

    state = {
        sectionList: []
    };

    componentDidMount() {

    }

    render() {
        const item = this.props.item.section.data;
        return (
            sectionBuilder(item)
        );
    }
}

export default Section;