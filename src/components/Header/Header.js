import React, {Fragment} from "react";
import {NavLink} from "react-router-dom"

const isMobile = /iPhone|iPad|iPod|Android/i.test(navigator.userAgent);

const Header = (props) => {
    const header = props.data;
    const logo = props.logo;
    const custom_pages = props.custom_pages;
    const DesktopHeader = (
        <nav>
            <ul>
                <li><NavLink to={{
                    pathname: "/", /*
                        hash: "#homeUID",
                        search: "?quick-search=elementID"*/
                }} exact>{header.home}</NavLink></li>
                {/*<li><NavLink to="/about-us">{header.about_us}</NavLink></li>*/}
                {/*<li><NavLink to="/products">{header.products}</NavLink></li>*/}
                {/*<li><NavLink to="/financing">{header.financing}</NavLink></li>*/}
                {
                    custom_pages.length &&
                    custom_pages.map((res, index) => {
                        const isPromo = res.label.toLowerCase().includes("promo");
                        return <li key={index + Math.random()}><NavLink
                            to={"/" + res.label.trim().replace(" ", "_").toLowerCase()} data-promo={isPromo}>{res.label}</NavLink></li>
                    })
                }
                {/*<li><NavLink to="/our-network">{header.our_network}</NavLink></li>*/}
                <li><NavLink to="/contact">{header.contact}</NavLink></li>
            </ul>
        </nav>
    );

    const MobileHeader = (
        <Fragment>
            <ul className="main-navigation">
                <li><NavLink to={{pathname: "/"}} exact>{header.home}</NavLink></li>
                <li><NavLink to="/contact">{header.contact}</NavLink></li>
            </ul>
            <ul className="custom-page-navigation">
                {
                    custom_pages.length &&
                    custom_pages.map((res, index) => {
                        const isPromo = res.label.toLowerCase().includes("promo");
                        return <li key={index + Math.random()}><NavLink
                            to={"/" + res.label.trim().replace(" ", "_").toLowerCase()} data-promo={isPromo}>{res.label}</NavLink></li>
                    })
                }
            </ul>
        </Fragment>
    );
    return (
        <header>
            <div className="logo">
                <NavLink to="/">
                    <img src={logo} alt=""/>
                </NavLink>
            </div>
            {
                isMobile ? MobileHeader : DesktopHeader
            }
        </header>
    );
};

export default Header;


