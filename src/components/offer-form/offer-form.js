import React, {Component} from "react";

import "./offer-form.css";

const token = "0abd677f-356b-4b36-a798-d85da76c5eee";

class OfferForm extends Component {
    state = {
        formObject: {
            name: "",
            phoneNo: ""
        }
    };

    render() {
        return (
            <div className="offer-form">
                <h2>Tinem legatura</h2>
                <div className="subscribe-form">
                    <input type="text" placeholder={"Numele dumneavoastra"} value={this.state.formObject.email}
                           onChange={e => this.setState({
                               formObject: {
                                   name: e.target.value,
                                   phoneNo: this.state.formObject.phoneNo
                               }
                           })}/>
                    <input type="text" placeholder={"Nr. de telefon"} value={this.state.formObject.email}
                           onChange={e => this.setState({
                               formObject: {
                                   name: this.state.formObject.name,
                                   phoneNo: e.target.value
                               }
                           })}/>
                    <button type="button" onClick={() => this.submitForm()}>Trimite</button>
                </div>
            </div>
        );
    }

    submitForm = () => {
        if(this.state.formObject.name.length && this.state.formObject.phoneNo.length) {
            window.Email.send("office@banzice.ro",
                "office@banzice.ro",
                "Contact telefonic - " + this.state.formObject.name,
                "Nume: " + this.state.formObject.name + " Numar telefon: " + this.state.formObject.phoneNo,
                {token: token})
        }
    }
}

export default OfferForm;
