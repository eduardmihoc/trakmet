import React from "react"
import AUX from "../../hoc/auxiliary";

const cockpit = (props) => {
    const contentList = props.contentList;
    const showContent = props.showContent;
    const styling = {
        button: {
            backgroundColor: showContent ? "red" : "green",
            color: "white",
            border: "1px solid blue",
            font: "inherit",
            padding: "8px",
            cursor: "pointer",
        }
    };
    //Styling
    const classes = [];
    if (contentList.length < 2) {
        classes.push("red")
    }

    if (contentList.length <= 1) {
        classes.push("bold");
        styling.button[":hover"] = {
            backgroundColor: "salmon",
            color: "black"
        }
    }
    return (
        <AUX>
            <h1>Hello world!</h1>
            <p className={classes.join(" ")}>Uuuuuu</p>
            <button
                onClick={props.toggleContentHandler}
                style={styling.button}>
                Toggle content
            </button>
            <button onClick={props.login}>Login</button>
        </AUX>
    );
};

export default cockpit;