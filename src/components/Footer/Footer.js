import React from "react";
import SubscriptionForm from "./components/subscription-form";

const sectionBuilder = (sectionItem) => {
    let section;
    switch (sectionItem.slice_type) {
        case "personal":
            return (
                <li key={Math.random()}>
                    <h2>{sectionItem.primary.title[0].text}</h2>
                    <p>{sectionItem.primary.name[0].text}</p>
                    <p>{sectionItem.primary.phone_number[0].text}</p>
                    <p>{sectionItem.primary.email[0].text}</p>
                    <div className="social-media">
                        {
                            sectionItem.items.map(res => {
                                return (
                                    <a key={Math.random()} href={res.url.url}><i className={res.social_media}></i>{res.label[0].text}</a>
                                )
                            })
                        }
                    </div>
                </li>
            );
        case "about":
            return (
                <li key={Math.random()}>
                    <h2>{sectionItem.primary.title[0].text}</h2>
                    <p>{sectionItem.primary.description[0].text}</p>
                </li>
            );
        case "contact":
            return (
                <li key={Math.random()}>
                    <h2>{sectionItem.primary.title[0].text}</h2>
                    <p>{sectionItem.primary.subtitle[0].text}</p>
                    {
                        sectionItem.primary.contact_info.map(res => {
                            return (
                                <p key={Math.random()}>{res.text}</p>
                            )
                        })
                    }
                    {
                        sectionItem.primary.image.url &&
                        <img src={sectionItem.primary.image.url} alt="" style={{height: "100px"}}/>
                    }
                </li>
            );
        case "subscribe":
            return (
                <li key={Math.random()}>
                    <SubscriptionForm placeholderObject={sectionItem.primary}/>
                </li>
            );

        default:
            section = <p>Test</p>;
    }
    return section;
};

const Footer = (props) => {
    const {data} = props;
    return (
        <footer>
            <nav>
                <ul>
                    {
                        data.map(res => {
                            return sectionBuilder(res)
                        })
                    }
                </ul>
            </nav>
        </footer>
    );
};

export default Footer;


