import React, {Component, Fragment} from "react";

const token = "0abd677f-356b-4b36-a798-d85da76c5eee";

export default class SubscriptionForm extends Component {

    state = {
        formObject: {
            name: "",
            email: ""
        }
    };


    render() {
        const sectionItem = this.props.placeholderObject;
        return (
            <Fragment>
                <h2>{sectionItem.title[0].text}</h2>
                <p>{sectionItem.description[0].text}</p>
                <div className="subscribe-form">
                    <input type="text" placeholder={sectionItem.subscriber_name[0].text}
                           value={this.state.formObject.name} onChange={e => this.setState({
                        formObject: {
                            name: e.target.value,
                            email: this.state.formObject.email
                        }
                    })}/>
                    <input type="text" placeholder={sectionItem.subscriber_email[0].text}
                           value={this.state.formObject.email} onChange={e => this.setState({
                        formObject: {
                            name: this.state.formObject.name,
                            email: e.target.value
                        }
                    })}/>
                    <button type="button"
                            onClick={() => this.submitForm()}>{sectionItem.subscribe_button[0].text}</button>
                </div>
            </Fragment>
        )
    }

    submitForm = () => {
        if (this.state.formObject.length && this.state.formObject.email.length) {
            window.Email.send("office@banzice.ro",
                "office@banzice.ro",
                "Subscriere - " + this.state.formObject.name,
                "Nume: " + this.state.formObject.name + " Email: " + this.state.formObject.email,
                {token: token})
        }
    }
}