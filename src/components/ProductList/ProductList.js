import React from "react";
import {Link} from "react-router-dom";

import "./ProductList.css"

const ProductList = (props) => {
    const {products, path} = props;
    return (
        <div className={"section products"}>
            <div className="item-container">
                {
                    products.map((product, index) => {
                        return (
                            <Link key={index + Math.random()}
                                  to={{
                                      pathname: path + "/product/" + encodeURI(product.id)
                                  }}>
                                <div className={"section-item " + (index % 2 && "align-right")}>
                                    <h2>{product.data.title[0].text}</h2>
                                    <p>{product.data.short_description[0].text}</p>

                                    {/*<span>{item.cta}</span>*/}
                                    <img
                                        src={product.data.small_image.url}
                                        alt=""/>
                                </div>
                            </Link>
                        )
                    })
                }
            </div>
        </div>
    )
};

export default ProductList;