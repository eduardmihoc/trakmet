import React, {Component} from "react";
import Person from "./Person/Person"

class People extends Component {
    constructor(props) {
        super(props);
        console.log('[People.js] Inside constructor', props);
    }

    componentWillMount() {
        console.log('[People.js] Inside componentWillMount()');
    }

    componentDidMount() {
        console.log('[People.js] Inside componentDidMount()');
    }

    shouldComponentUpdate(nextProps, nextState){
        console.log('[Update People.js] Inside shouldComponentUpdate()', nextProps, nextState);
        return true;
    }

    componentWillUpdate(nextProps, nextState){
        console.log('[will Update People.js] Inside componentWillUpdate()', nextProps, nextState);
    }

    componentDidUpdate(nextProps, nextState){
        console.log('[did Update People.js] Inside componentDidUpdate()', nextProps, nextState);
    }

    render() {
        console.log('[People.js] Inside render()');

        return this.props.people.map((item, index) => {
            return <Person
                key={item.id}
                name={item.name}
                age={item.age}
                position={index}
                click={() => this.props.deleteItem(index)}
                nameChange={(event) => this.props.nameChangeHandler(event, item.id)}>
                {item.hobbies}
            </Person>
        });
    }
}

export default People;
