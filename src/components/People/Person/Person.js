import React, {Component} from 'react';

import PropTypes from "prop-types";
import AUX from "../../../hoc/auxiliary";
import {withClassStatefull as withClassS} from "../../../hoc/withClass";
import {AuthContext} from "../../../containers/App"
import './Person.css';

class Person extends Component {
    constructor(props) {
        super(props);
        console.log('[Person.js] Inside constructor', props);
        this.inputElement = React.createRef();
    }

    componentWillMount() {
        console.log('[Person.js] Inside componentWillMount()');
    }

    componentDidMount() {
        console.log('[Person.js] Inside componentDidMount()');
        if (this.props.position === 0) {
            this.inputElement.current.focus();
        }
    }

    componentWillReceiveProps(nextProps) {
        console.log('[Person.js] Inside componentWillReceiveProps()', nextProps);
    }

    render() {
        console.log('[Person.js] Inside render()');

        const props = this.props;

        return (
            <AUX>
                <AuthContext.Consumer>
                    {auth => auth && <p>I'm authenticated</p>}
                </AuthContext.Consumer>

                <p onClick={props.click}>My name is {props.name} and I am {props.age} years old</p>
                <p>{props.children}</p>
                <input
                    ref={this.inputElement}
                    type="text"
                    onChange={props.nameChange}
                    value={props.name}/>
            </AUX>
        )
    }
}

Person.propTypes = {
    click: PropTypes.func,
    name: PropTypes.string,
    age: PropTypes.number,
    changed: PropTypes.func
};

export default withClassS(Person, "Person");