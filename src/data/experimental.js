/**
 * Returns the static page object with the zone slices populated
 * @param {object} ctxConfig
 * @param {object} userData
 * @param {object} res
 * */
const getStaticPageZoneSlices = (ctxConfig, userData, res) => {
    let zonePromises = [];

    res.data.body.forEach(item => {
        if (item.slice_type === "zone" && item.primary.zone_item.id) {
            zonePromises.push(
                query(ctxConfig,
                    predicates.any("document.id", safePick(item, "primary", "zone_item", "id")))
                    .then(res => {
                        let itemPromises = [];
                        const zoneItems = safePick(res, "results", "0", "data", "body", "0", "items") || [];

                        item.primary.zone_item.creativeItems = [];
                        item.title = safePick(res, "results[0]", "data", "title[0]", "text");
                        item.zoneItemOrder = zoneItems.map(data => {
                            return data.item.id;
                        }).filter(Boolean);

                        //get all games zone data
                        itemPromises.push(query(
                            ctxConfig,
                            predicates.any("document.id", safePick(item, "zoneItemOrder")),
                            ["provider.status"])
                            .then(res => {
                                //append the valid games array to coresponding zone
                                item.primary.zone_item.creativeItems = (res.results || []).filter(game => validate(game, userData));
                            })
                        );
                        return Promise.all(itemPromises);
                    })
            );
        }
    });

    //return the original response object along with the zone slice data concatenated and reordered items to each of the zone slices
    return Promise.all(zonePromises).then(() => res).then(response => {
        res.data.body.forEach(item => {
            if (safePick(item, "primary", "zone_item", "id") && safePick(item, "slice_type") === "zone" && safePick(item, "primary", "zone_item", "creativeItems")) {
                item.primary.zone_item.creativeItems = orderList(item.primary.zone_item.creativeItems, item.zoneItemOrder);
            }
        });
        return response;
    });
};