import React, {Component} from "react";
import Prismic from 'prismic-javascript';
import {RichText} from 'prismic-reactjs';
import prismicMoc from "./homepageMockData";

const apiEndpoint = 'https://trakmet.prismic.io/api/v2';
const accessToken = "MC5XMEhJd3lNQUFKRFR6bWow.77-9L04KLu-_vQJ377-977-9dVJkWO-_vUrvv73vv73vv73vv73vv73vv73vv71F77-9OO-_ve-_ve-_vSPvv71w";

export const apiEndpointObject = Prismic.api(apiEndpoint, {accessToken});

const getMasterRef = () => {
    return apiEndpointObject.then(api => {
        return api.masterRef.ref;
    })
};

const query = (predicate, predicateQuery, linkFetch = [1]) => {
    return apiEndpointObject.then(api => {
        return api.query(Prismic.Predicates.at(`document.${predicate}`, predicateQuery), {
            'fetchLinks': [...linkFetch],
            pageSize: 100
        });
    });
};

const queryById = (id) => {
    return apiEndpointObject.then(api => {
        return api.getByID(id);
    });
};

//TODO properly consume promises
const getHomepageContent = async () => {
    const predicate = "type";
    const queryPredicate = "homepage";
    const linkFetch = ['section.title', 'section.description', 'section.type', 'section.cta', 'section.type', 'section.background-image', 'section.logo'];

    return query(predicate, queryPredicate, linkFetch).then(res => {
        let content = res.results[0];
        const sections = content.data.sections;
        let sectionPromises = [];
        if (sections.length) {
            sectionPromises = sections.map((value) => {
                value.sectionPromises = queryById(value.section.id).then(res => {
                    if (res.data.body.length) {
                        return res.data.body.map(item => {
                            switch (item["slice_type"]) {
                                case "hero":
                                    item.items = item.items.map(item => item['hero-thumb']);
                                    item.backHandler = "Test";
                                    return item;
                                case "products":
                                    const productListInfo = item.items.map(res => {
                                        return queryById(res["product-list"].id).then(res => {
                                            return res;
                                        });
                                    });
                                    return Promise.all(productListInfo).then(res => {
                                        return res;
                                    });
                                case "list":
                                    value.section.data.listItems = item.items;
                                    break;
                                case "flags":
                                    const flagList = item.items.map(res => {
                                        return queryById(res["flag-item"].id).then(res => {
                                            return res.data;
                                        });
                                    });
                                    return Promise.all(flagList).then(res => {
                                        return res;
                                    });
                                default:
                                    return [{test: "Test"}]
                            } return void 0;
                        });
                    } else {
                        return [{test: "Test"}]
                    }
                });
                return value;
            });
            content.data.sections = sections;
        }

        return Promise.all(sectionPromises).then(res => {
            const productsData = res.map(result => {
                return result.sectionPromises.then(res => {
                    try {
                        return res[0].then(res => {
                            result.section.data.infoList = [...res];
                            return {
                                ...result
                            };
                        })
                    } catch (e) {

                    }
                    result.section.data.info = {...res[0]};
                    return {
                        ...result
                    };
                });
            });

            //TODO properly consume products data
            //wait for products data to be pulled from Prismic and then return the content for the app to render
            return Promise.all(productsData).then(res => {
                return content;
            });
        });
    });
};

const getProductContent = (id) => {
    return queryById(id).then(res => {
        return res.data;
    })
};

const getAboutUsPage = () => {
    const predicate = "type";
    const queryPredicate = "about_us";

    return query(predicate, queryPredicate).then(res => {
        return res.results[0].data;
    })
};

const getFinancingPage = () => {
    const predicate = "type";
    const queryPredicate = "financin";

    return query(predicate, queryPredicate).then(res => {
        return res.results[0].data;
    })
};

const getContactPage = () => {
    const predicate = "type";
    const queryPredicate = "contact";

    return query(predicate, queryPredicate).then(res =>{
        return res.results[0].data;
    })
};

const getNavigation = () => {
    const predicate = "type";
    const queryPredicate = "navigation";

    return query(predicate, queryPredicate).then(res => {
        const data = res.results[0].data;
        const custom_page_array = data.custom_pages.map(custom_page => {
            return queryById(custom_page.page_link.id).then(res => {
                return {
                    label: custom_page.page_label[0].text,
                    data: res.data
                }
            })
        });

        const header = {
            home: data.home[0].text,
            about_us: data["about_us"][0].text,
            products: data.products[0].text,
            financing: data.financing[0].text,
            our_network: data["our_network"][0].text,
            contact: data.contact[0].text
        };

        return {
            logo: data.logo.url,
            header,
            custom_page_array,
            footer: data.body
        };
    })
};

const getAllProducts = async () => {
    const predicate = "type";
    const queryPredicate = "product";

    return query(predicate, queryPredicate).then(res => {
        return res.results;
    })
};

const getContent = async (id) => {
    let content;
    switch (id) {
        case "homepage":
            content = await getHomepageContent();
            return content;
        case "products":
            content = await getAllProducts();
            return content;
        case "about_us":
            content = await getAboutUsPage();
            return content;
        case "financing":
            content = await getFinancingPage();
            return content;
        case "contact":
            content = await getContactPage();
            return content;
        default:
            return {};
    }
};

export class PrismicContentHandler extends Component {
    state = {
        doc: null
    };

    // Link Resolver
    linkResolver(doc) {
        // Define the url depending on the document type
        if (doc.type === 'homepage') {
            return '/homepage/' + doc.uid;
        } else if (doc.type === 'blog_post') {
            return '/blog/' + doc.uid;
        }

        // Default to homepage
        return '/';
    }

    async componentWillMount() {
    }

    render() {
        if (this.state.doc) {
            const document = this.state.doc.data;
            return (
                <div>
                    <h1>{RichText.asText(document.title)}</h1>
                    <img alt="cover" src={document.background.url}/>
                    {RichText.render(document.description, this.linkResolver)}
                </div>
            );
        }
        return <h1>Loading...</h1>;
    }
}

const contentHandler = () => {
    return prismicMoc;
};

export {
    getMasterRef,
    getContent,
    getNavigation,
    getProductContent
};

export default contentHandler;