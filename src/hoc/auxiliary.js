//return the children under the auxiliary High Order Component to render the elements without a parent
const auxiliary = (props) => props.children;

export default auxiliary;